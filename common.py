#!/usr/bin/env python3

from enum import Enum
from argparse import ArgumentParser
import logging
import json

JSON_FILE_PATH = "providers.json"
OUTPUT_INDENTATION = "\t"


class Category(Enum):
	AUTOMATICALLY_CHOSEN = "A"
	MANUALLY_SELECTABLE = "B"
	USABLE_FOR_AUTOCOMPLETE = "C"

class ApplicationArgumentParser(ArgumentParser):
	"""This is a parser for the arguments provided to run the application."""

	def __init__(self):
		super().__init__()

		self.version = "%%prog 0.1"
		self.usage = "Usage: %%prog [-h | [-q | -d] [-A | -B | -C] [providers]]"

		self.add_argument(
			"-q",
			"--quiet",
			help="log only errors",
			action="store_const",
			dest="log_level",
			const=logging.ERROR,
			default=logging.INFO,
		)

		self.add_argument(
			"-d",
			"--debug",
			help="log debug output",
			action="store_const",
			dest="log_level",
			const=logging.DEBUG,
			default=logging.INFO,
		)

		self.add_argument(
			"-s",
			"--simple",
			help="output list of provider domains",
			action="store_true",
			dest="simple",
		)

		self.add_argument(
			"-A",
			"--category-A",
			help="extract providers of category A",
			action="store_const",
			dest="category",
			const=Category.AUTOMATICALLY_CHOSEN,
			default=Category.USABLE_FOR_AUTOCOMPLETE,
		)

		self.add_argument(
			"-B",
			"--category-B",
			help="extract providers of category B",
			action="store_const",
			dest="category",
			const=Category.MANUALLY_SELECTABLE,
			default=Category.USABLE_FOR_AUTOCOMPLETE,
		)

		self.add_argument(
			"-C",
			"--category-C",
			help="extract providers of category C",
			action="store_const",
			dest="category",
			const=Category.USABLE_FOR_AUTOCOMPLETE,
			default=Category.USABLE_FOR_AUTOCOMPLETE,
		)

		self.add_argument(
			"providers",
			help="domain names of providers being extracted",
			nargs='*',
		)
