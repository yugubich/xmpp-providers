#!/usr/bin/env python3

"""
This script filters the provider list and creates a list of extracted providers.
"""

from datetime import datetime, timedelta

from common import *

criteriaA = {
	"inBandRegistration": lambda v : v,
	"ratingXmppComplianceTester": lambda v : v >= 90,
	"ratingImObservatoryClientToServer": lambda v : "A" in v,
	"ratingImObservatoryServerToServer": lambda v : "A" in v,
	"maximumHttpFileUploadFileSize": lambda v : v == 0 or v >= 20,
	"maximumHttpFileUploadTotalSize": lambda v : v == 0 or v >= 100,
	"maximumHttpFileUploadStorageTime": lambda v : v == 0 or v >= 7,
	"maximumMessageArchiveManagementStorageTime": lambda v : v == 0 or v >= 7,
	"professionalHosting": lambda v : v,
	"freeOfCharge": lambda v : v,
	"legalNotice": lambda v : len(v) != 0,
	"serverLocations": lambda v : len(v) != 0,
	"legalNotice": lambda v : len(v) != 0,
	"legalNotice": lambda v : len(v) != 0,
	"legalNotice": lambda v : len(v) != 0,
	"groupChatSupport,chatSupport,emailSupport": lambda v1, v2, v3 : len(v1) != 0 or len(v2) != 0 or len(v3) != 0,
	"onlineSince": lambda v : datetime.fromisoformat(v) < datetime.now() - timedelta(365),
}

criteriaB = {
	"inBandRegistration,registrationWebPage": lambda v1, v2 : v1 or len(v2) != 0,
	"ratingXmppComplianceTester": lambda v : v >= 90,
	"ratingImObservatoryClientToServer": lambda v : "A" in v,
	"ratingImObservatoryServerToServer": lambda v : "A" in v,
	"maximumHttpFileUploadFileSize": lambda v : v >= 0,
	"maximumHttpFileUploadTotalSize": lambda v : v >= 0,
	"maximumHttpFileUploadStorageTime": lambda v : v >= 0,
	"maximumMessageArchiveManagementStorageTime": lambda v : v >= 0,
}

criteriaC = {}

def create_simple_output(jid, properties):
	"""Creates a simple output for the properties of a provider consisting only of the information relevant to the client.

	Parameters
	----------
	jid : str
		JID of the provider
	properties : dict
		properties of the provider

	Returns
	-------
	dict
		only consisting of relevant properties
	"""

	del properties["lastCheck"]
	new_properties = {"jid": jid}

	for property_name, property_content in properties.items():
		if "content" in property_content:
			new_properties[property_name] = property_content["content"]
		else:
			new_properties[property_name] = dict()
			for language_code, language_specific_content in property_content.items():
				content = language_specific_content["content"]
				if len(content) != 0 :
					new_properties[property_name][language_code] = language_specific_content["content"]

	return new_properties

def filter_provider(category, properties):
	"""Filters properties by a passed category.

	Parameters
	----------
	category : common.Category
		category used for filtering
	properties : dict
		properties of the provider

	Returns
	-------
	bool
		whether the provider belongs to the category
	"""

	if category == Category.USABLE_FOR_AUTOCOMPLETE:
		return check_properties(category, properties, criteriaC)

	if category == Category.MANUALLY_SELECTABLE:
		return check_properties(category, properties, criteriaB)

	if category == Category.AUTOMATICALLY_CHOSEN:
		return check_properties(category, properties, criteriaA)

	return False

def check_properties(category, properties, criteria):
	"""Checks if properties meet specific criteria.

	Parameters
	----------
	category : common.Category
		category used for filtering
	properties : dict
		properties of the provider
	criteria : dict
		criteria that are checked

	Returns
	-------
	bool
		whether all properties meet the criteria
	"""

	check_succeeded = True

	for property_string, criterion in criteria.items():
		property_names = property_string.split(",")
		property_values = []

		for property_name in property_names:
			property_values.append(properties[property_name])

		if not criterion(*property_values):
			logging.debug("  %s: %s not meeting the criterion for category %s" % (", ".join(property_names), str(property_values)[1:-1], category.value))

			if check_succeeded:
				check_succeeded = False

	return check_succeeded

if __name__ == "__main__":
	arguments = ApplicationArgumentParser().parse_args()
	logging.basicConfig(level=arguments.log_level, format="%(levelname)-8s %(message)s")

	category = arguments.category

	with open(JSON_FILE_PATH, "r") as json_file:
		try:
			providers = json.load(json_file)

			selected_providers = arguments.providers
			selected_providers_count = len(selected_providers)
			providers_selected = selected_providers_count != 0

			filtered_providers = []

			for jid, properties in providers.items():
				if not providers_selected or jid in selected_providers:
					logging.debug("%s: Filtering" % jid)

					properties = create_simple_output(jid, properties)

					if filter_provider(category, properties):
						filtered_providers.append(properties)

			providers_count = selected_providers_count if providers_selected else len(providers)
			logging.debug("RESULT: %s of %s providers in category %s" % (len(filtered_providers), providers_count, category.value))
			logging.debug("The criteria are specified in the README: https://invent.kde.org/melvo/xmpp-providers#criteria")

			if arguments.simple:
				filtered_providers = [provider['jid'] for provider in filtered_providers]

			# A newline is appended because Python's JSON module does not add one.
			formatted_json_string = json.dumps(filtered_providers, indent=OUTPUT_INDENTATION) + "\n"

			json_file_path_with_suffix = "%s-%s.json" % (JSON_FILE_PATH.split(".")[0], category.value)
			with open(json_file_path_with_suffix, "w") as filtered_json_file:
				filtered_json_file.seek(0)
				filtered_json_file.write(formatted_json_string)
				filtered_json_file.truncate()
				logging.info("'%s' has been created" % json_file_path_with_suffix)
		except json.decoder.JSONDecodeError as e:
			logging.error("'%s' has invalid JSON syntax: %s in line %s at column %s" % (JSON_FILE_PATH, e.msg, e.lineno, e.colno))
