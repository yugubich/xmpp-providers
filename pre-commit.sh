#!/bin/sh
#
# This script runs all Git pre-commit hook scripts.
#
# For adding an additional hook script, append the last line by " && \" and add a new line for the script path.
# Example:
# ./script1.py && \
# ./script2.py

./prettify.py
